## Справочник продуктов с указанием хлебных единиц

### Общая информация
Серверное приложение на django, включает в себя два django app - `product_catalog` и `api`.

`product_catalog` - Классический бэкенд для сайта http://xecalc.ru/

`api` - api бэкенд на основе drf, для приложения.


### Запуск
`docker-compose up --build`

### Тесты
Тесты на данный момент покрывают только api.

`docker exec xe_calc_backend python manage.py test`