from django.urls import path

from api.views import (
    SearchProductListApiView,
    PortionsListApiView,
    ProductAndPortions,
    BrandProductsList,
    FeedbackCreateApiView,
    ProductComplaintCreateView,
    SlugifierAPIView,
)

app_name = 'api'
urlpatterns = [
    path('feedback/', FeedbackCreateApiView.as_view(), name='feedback'),
    path('products/search/', SearchProductListApiView.as_view(), name='search'),
    path('products/create-complaint/', ProductComplaintCreateView.as_view(), name='product_complaint'),
    path('products/brand/<slug:brand_slug>/', BrandProductsList.as_view(), name='brand_products'),
    path('products/<uuid:product>/portions/', PortionsListApiView.as_view(), name='portions_by_id'),
    path('products/<slug:slug>/product-with-portions/', ProductAndPortions.as_view(), name='product_with_portions'),
    path('slugifier/', SlugifierAPIView.as_view(), name='slugifier'),
]
