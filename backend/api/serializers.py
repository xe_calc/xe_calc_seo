from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from product_catalog.models import Product, ProductPortion, Feedback, ProductComplaint


class ProductSearchSerializer(serializers.ModelSerializer):
    brand_name = serializers.SerializerMethodField()

    def get_brand_name(self, obj):
        if obj.brand:
            return obj.brand.name
        return None

    class Meta:
        model = Product
        fields = ['uuid', 'name', 'brand_name', 'slug']


class ProductPortionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductPortion
        fields = [
            'id',
            'name',
            'for_weighing',
            'calories',
            'fat',
            'protein',
            'carbohydrates',
            'xe_count',
        ]


class ProductWithPortionsSerializer(serializers.ModelSerializer):
    portions = ProductPortionSerializer(many=True, read_only=True)
    brand = serializers.SerializerMethodField()

    def get_brand(self, obj):
        if obj.brand_id:
            return {'slug': obj.brand.slug, 'name': obj.brand.name}
        return None

    class Meta:
        model = Product
        fields = [
            'name',
            'brand',
            'portions',
        ]


class BrandProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'slug']


class FeedbackCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feedback
        fields = ['contacts', 'text']


class ComplaintCreateSerializer(serializers.ModelSerializer):
    product = serializers.SlugRelatedField(slug_field='slug', queryset=Product.objects.all())

    def validate(self, attrs):
        product = attrs.get('product')
        portion = attrs.get('portion')

        if product and portion and portion.product_id != product.id:
            raise ValidationError({'portion': 'Данная порция не подходит для этого продукта'})
        return attrs

    class Meta:
        model = ProductComplaint
        fields = ['product', 'portion', 'text']
