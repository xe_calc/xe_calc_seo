import math

from django.conf import settings
from django.contrib.postgres.search import SearchVector
from pytils.translit import slugify
from rest_framework.exceptions import NotFound
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    get_object_or_404
)
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import (
    ProductSearchSerializer,
    ProductPortionSerializer,
    ProductWithPortionsSerializer,
    BrandProductSerializer, FeedbackCreateSerializer, ComplaintCreateSerializer,
)
from product_catalog.models import Product, ProductPortion, ProductBrand


class SearchProductPaginator(LimitOffsetPagination):
    default_limit = settings.PRODUCT_MAX_COUNT_FOR_SEARCH
    max_limit = settings.PRODUCT_MAX_COUNT_FOR_SEARCH


class SearchProductListApiView(ListAPIView):
    serializer_class = ProductSearchSerializer
    queryset = Product.objects.all()
    pagination_class = SearchProductPaginator

    def get_queryset(self):
        name = self.request.GET.get('name')
        if name:
            return (
                self.queryset
                .annotate(search=SearchVector('name', 'brand__name'))
                .filter(search=name)
                .select_related('brand')
                .only('name', 'slug', 'brand__name')
                .order_by('-brand_id', 'id')
            )
        return self.queryset.none()


class PortionsListApiView(ListAPIView):
    serializer_class = ProductPortionSerializer
    queryset = ProductPortion.objects.all()

    def get_queryset(self):
        product_uuid = self.kwargs.get('product')
        return self.queryset.filter(product__uuid=product_uuid).order_by('-for_weighing')


class ProductAndPortions(RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductWithPortionsSerializer
    lookup_field = 'slug'


class BrandProductsPaginator(PageNumberPagination):
    page_size = 10

    def get_paginated_response(self, data):
        return Response({
            'pages_count': math.ceil(self.page.paginator.count / self.page_size),
            'results': data
        })


class BrandProductsList(ListAPIView):
    queryset = Product.objects.all()
    pagination_class = BrandProductsPaginator
    brand = None

    def _get_product_brand(self):
        slug = self.kwargs.get('brand_slug')
        if slug:
            return get_object_or_404(ProductBrand, slug=slug)
        raise NotFound()

    def get_queryset(self):
        self.brand = self._get_product_brand()
        qs = super().get_queryset()
        return qs.filter(brand=self.brand).order_by('name')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        result = {
            'brand_name': self.brand.name,
            'products': []
        }

        if page is not None:
            result['products'] = BrandProductSerializer(page, many=True).data
            return self.get_paginated_response(result)

        result['products'] = BrandProductSerializer(queryset, many=True).data
        return Response(result)


class FeedbackCreateApiView(CreateAPIView):
    serializer_class = FeedbackCreateSerializer


class ProductComplaintCreateView(CreateAPIView):
    serializer_class = ComplaintCreateSerializer

class SlugifierAPIView(APIView):
    def get(self, request, *args, **kwargs):
        return Response({'slug': slugify(request.GET.get('word'))})