import uuid
from decimal import Decimal

from django.conf import settings
from rest_framework import status
from rest_framework.test import APITestCase

from product_catalog.models import Product, ProductBrand, ProductPortion, Feedback, ProductComplaint


class TestSearch(APITestCase):

    def setUp(self):
        self.bread_brand = ProductBrand.objects.create(name='Бренд', slug='brand')
        self.chicken_brand = ProductBrand.objects.create(name='Цыпа', slug='tsypa')

        self.bread_product = Product.objects.create(name='Хлеб', slug='hleb')
        self.bread_product_with_brand = Product.objects.create(name='Хлеб', slug='hleb-brand', brand=self.bread_brand)
        self.chicken_product = Product.objects.create(name='Курица', slug='kuritsa', brand=self.chicken_brand)

    def test_response_ok(self):
        response = self.client.get('/api/products/search/?name=Хлеб')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_product_fields(self):
        response = self.client.get('/api/products/search/?name=Курица')

        response_product = response.data['results'][0]

        self.assertEqual(len(response_product.keys()), 4)
        self.assertEqual(response_product['uuid'], str(self.chicken_product.uuid))
        self.assertEqual(response_product['slug'], self.chicken_product.slug)
        self.assertEqual(response_product['name'], self.chicken_product.name)
        self.assertEqual(response_product['brand_name'], self.chicken_brand.name)

    def test_search(self):
        response = self.client.get('/api/products/search/?name=Хлеб')

        self.assertEqual(response.data['count'], 2)
        self.assertListEqual(
            [data['slug'] for data in response.data['results']],
            [self.bread_product.slug, self.bread_product_with_brand.slug]
        )

    def test_product_without_brands_in_top(self):
        Product.objects.create(name='Хлеб', slug='hleb-brand2', brand=self.bread_brand)
        Product.objects.create(name='Хлеб бородинский', slug='hle')

        response = self.client.get('/api/products/search/?name=Хлеб')

        self.assertEqual(response.data['results'][0]['brand_name'], None)
        self.assertEqual(response.data['results'][1]['brand_name'], None)

    def test_default_limit_products(self):
        for i in range(0, 100):
            Product.objects.create(name='Хлеб бородинский', slug=f'hleb-{i}')

        response = self.client.get('/api/products/search/?name=Хлеб')

        self.assertEqual(len(response.data['results']), settings.PRODUCT_MAX_COUNT_FOR_SEARCH)

    def test_pagination(self):
        limit = 10
        offset = 5
        product_slugs = []
        for i in range(1, 20):
            product = Product.objects.create(name='Осьминог', slug=f'osm-{i}')
            if len(product_slugs) < limit and i >= offset + 1:
                product_slugs.append(product.slug)

        response = self.client.get(f'/api/products/search/?name=Осьминог&offset={offset}&limit={limit}')

        self.assertEqual(len(response.data['results']), limit)
        self.assertEqual([prod['slug'] for prod in response.data['results']], product_slugs)

    def test_overage_limit(self):
        for i in range(1, 100):
            Product.objects.create(name='Осьминог', slug=f'osm-{i}')

        response = self.client.get('/api/products/search/?name=Осьминог&limit=200')

        self.assertEqual(len(response.data['results']), settings.PRODUCT_MAX_COUNT_FOR_SEARCH)

    def test_without_name_param(self):
        response = self.client.get('/api/products/search/')

        self.assertEqual(len(response.data['results']), 0)

    def test_name_param_is_empty(self):
        response = self.client.get('/api/products/search/?name=')

        self.assertEqual(len(response.data['results']), 0)

    def test_products_not_exists(self):
        response = self.client.get('/api/products/search/?name=Нет такого')

        self.assertEqual(len(response.data['results']), 0)


class PortionsApiTestCase(APITestCase):

    def setUp(self):
        self.bread_product = Product.objects.create(name='Хлеб', slug='hleb')
        self.bread_portion_100gr = ProductPortion.objects.create(
            name='100гр',
            for_weighing=True,
            calories=50,
            fat=100,
            protein=150,
            carbohydrates=300,
            xe_count=5,
            product=self.bread_product,
        )
        self.bread_portion_1_item = ProductPortion.objects.create(
            name='1 шт',
            for_weighing=False,
            calories=800,
            fat=555,
            protein=300,
            carbohydrates=600,
            xe_count=100,
            product=self.bread_product,
        )

        self.chicken_product = Product.objects.create(name='Курица', slug='kuritsa')
        self.chicken_portion_100gr = ProductPortion.objects.create(
            name='100гр',
            for_weighing=True,
            calories=50,
            fat=100,
            protein=150,
            carbohydrates=300,
            xe_count=5,
            product=self.chicken_product,
        )
        self.chicken_portion_1_item = ProductPortion.objects.create(
            name='1 шт',
            for_weighing=False,
            calories=500,
            fat=200,
            protein=180,
            carbohydrates=300,
            xe_count=100,
            product=self.chicken_product,
        )
        self.chicken_portion_05_item = ProductPortion.objects.create(
            name='50гр',
            for_weighing=True,
            calories=50,
            fat=100,
            protein=150,
            carbohydrates=300,
            xe_count=5,
            product=self.chicken_product,
        )

    def test_response_ok(self):
        response = self.client.get(f'/api/products/{self.chicken_product.uuid}/portions/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_portions_only_select_product(self):
        chicken_portions_ids = [
            self.chicken_portion_100gr.id,
            self.chicken_portion_05_item.id,
            self.chicken_portion_1_item.id,
        ]
        response = self.client.get(f'/api/products/{self.chicken_product.uuid}/portions/')

        self.assertListEqual(chicken_portions_ids, [result['id'] for result in response.data])

    def test_portions_for_weight_in_top(self):
        response = self.client.get(f'/api/products/{self.chicken_product.uuid}/portions/')

        self.assertListEqual(
            [response.data[0]['id'], response.data[1]['id']],
            [self.chicken_portion_100gr.id, self.chicken_portion_05_item.id]
        )

    def test_structure_portions(self):
        test_fields = {'id', 'name', 'for_weighing', 'calories', 'fat', 'protein', 'carbohydrates', 'xe_count'}

        def assert_structure(db_portion, response_portion):
            db_portion.refresh_from_db()
            self.assertSetEqual(set(response_portion.keys()), test_fields)

            for field in test_fields:
                if isinstance(getattr(db_portion, field), Decimal):
                    self.assertEqual(getattr(db_portion, field), Decimal(response_portion[field]))
                else:
                    self.assertEqual(getattr(db_portion, field), response_portion[field])

        response = self.client.get(f'/api/products/{self.bread_product.uuid}/portions/')

        portion_100_gr_response = response.data[0]
        portion_1_item_response = response.data[1]

        assert_structure(self.bread_portion_100gr, portion_100_gr_response)
        assert_structure(self.bread_portion_1_item, portion_1_item_response)

    def test_product_without_portions(self):
        self.bread_product.portions.all().delete()

        response = self.client.get(f'/api/products/{self.bread_product.uuid}/portions/')

        self.assertEqual(len(response.data), 0)

    def test_product_not_exist(self):
        response = self.client.get(f'/api/products/{uuid.uuid4()}/portions/')

        self.assertEqual(len(response.data), 0)


class BrandProductsApi(APITestCase):

    def test_ok(self):
        brand = ProductBrand.objects.create(name='Бренд', slug='brand-1')
        bread_product = Product.objects.create(name='Хлеб', slug='hleb', brand=brand)
        cheese_product = Product.objects.create(name='Сыр', slug='syr', brand=brand)

        response = self.client.get(f'/api/products/brand/{brand.slug}/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['pages_count'], 1)
        self.assertDictEqual(data['results']['products'][0], {'slug': cheese_product.slug, 'name': cheese_product.name})
        self.assertDictEqual(data['results']['products'][1], {'slug': bread_product.slug, 'name': bread_product.name})
        self.assertEqual(data['results']['brand_name'], brand.name)

    def test_only_selected_slug_brand(self):
        brand = ProductBrand.objects.create(name='Бренд1', slug='brand-1')
        brand_one = ProductBrand.objects.create(name='Бренд2', slug='brand-2')

        bread_product = Product.objects.create(name='Хлеб', slug='hleb', brand=brand)
        Product.objects.create(name='Сыр', slug='syr', brand=brand_one)

        response = self.client.get(f'/api/products/brand/{brand.slug}/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['pages_count'], 1)
        self.assertDictEqual(data['results']['products'][0], {'slug': bread_product.slug, 'name': bread_product.name})
        self.assertEqual(data['results']['brand_name'], brand.name)

    def test_pagination(self):
        brand = ProductBrand.objects.create(name='Бренд', slug='brand-1')
        for n in range(1, 26):
            Product.objects.create(name=f'Хлеб_{n}', slug=f'hleb_{n}', brand=brand)

        response = self.client.get(f'/api/products/brand/{brand.slug}/')
        data = response.data
        self.assertEqual(data['pages_count'], 3)
        self.assertEqual(data['results']['brand_name'], brand.name)
        self.assertEqual(len(data['results']['products']), 10)

        response = self.client.get(f'/api/products/brand/{brand.slug}/?page=2')
        data = response.data
        self.assertEqual(data['pages_count'], 3)
        self.assertEqual(data['results']['brand_name'], brand.name)
        self.assertEqual(len(data['results']['products']), 10)

        response = self.client.get(f'/api/products/brand/{brand.slug}/?page=3')
        data = response.data
        self.assertEqual(data['pages_count'], 3)
        self.assertEqual(data['results']['brand_name'], brand.name)
        self.assertEqual(len(data['results']['products']), 5)

    def test_brand_not_found(self):
        response = self.client.get('/api/products/brand/not_found/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_products_empty(self):
        brand = ProductBrand.objects.create(name='Бренд', slug='brand-1')
        response = self.client.get(f'/api/products/brand/{brand.slug}/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data['pages_count'], 0)
        self.assertEqual(data['results']['products'], [])
        self.assertEqual(data['results']['brand_name'], brand.name)


class FeedbackCreateTestCase(APITestCase):

    def test_ok(self):
        response = self.client.post(
            '/api/feedback/',
            {'contacts': 'blabla@mail.com', 'text': 'bla bla bla'}
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, {'contacts': 'blabla@mail.com', 'text': 'bla bla bla'})
        self.assertEqual(Feedback.objects.all().count(), 1)

        feedback = Feedback.objects.all().first()
        self.assertEqual(feedback.contacts, 'blabla@mail.com')
        self.assertEqual(feedback.text, 'bla bla bla')

    def test_without_contacts(self):
        response = self.client.post(
            '/api/feedback/',
            {'contacts': '', 'text': 'bla bla bla'}
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'contacts': ['Это поле не может быть пустым.']})

        response = self.client.post(
            '/api/feedback/',
            {'text': 'bla bla bla'}
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'contacts': ['Обязательное поле.']})

    def test_without_text(self):
        response = self.client.post(
            '/api/feedback/',
            {'contacts': 'blabla@mail.com', 'text': ''}
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'text': ['Это поле не может быть пустым.']})

        response = self.client.post(
            '/api/feedback/',
            {'contacts': 'blabla@mail.com'}
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'text': ['Обязательное поле.']})

    def test_empty_body(self):
        response = self.client.post('/api/feedback/', {})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {'contacts': ['Обязательное поле.'], 'text': ['Обязательное поле.']}
        )


class ProductComplaintTestCase(APITestCase):

    def setUp(self):
        self.bread_product = Product.objects.create(name='Хлеб', slug='hleb')
        self.bread_portion_100gr = ProductPortion.objects.create(
            name='100гр',
            for_weighing=True,
            calories=50,
            fat=100,
            protein=150,
            carbohydrates=300,
            xe_count=5,
            product=self.bread_product,
        )
        self.bread_portion_unit = ProductPortion.objects.create(
            name='1 штука',
            for_weighing=False,
            calories=50,
            fat=100,
            protein=150,
            carbohydrates=300,
            xe_count=5,
            product=self.bread_product,
        )

    def test_ok(self):
        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'product': self.bread_product.slug,
                'portion': self.bread_portion_100gr.id,
                'text': 'Это жалоба'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.data,
            {
                'product': self.bread_product.slug,
                'portion': self.bread_portion_100gr.id,
                'text': 'Это жалоба'
            }
        )
        self.assertEqual(ProductComplaint.objects.all().count(), 1)

        complaint = ProductComplaint.objects.all().first()
        self.assertEqual(complaint.product, self.bread_product)
        self.assertEqual(complaint.portion, self.bread_portion_100gr)
        self.assertEqual(complaint.text, 'Это жалоба')
        self.assertFalse(complaint.solved)

    def test_portion_other_product(self):
        meat = Product.objects.create(name='Мясо', slug='myaso')
        meat_portion_100gr = ProductPortion.objects.create(
            name='100гр',
            for_weighing=True,
            calories=50,
            fat=100,
            protein=150,
            carbohydrates=300,
            xe_count=5,
            product=meat,
        )
        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'product': self.bread_product.slug,
                'portion': meat_portion_100gr.id,
                'text': 'Это жалоба'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {
                'portion': ['Данная порция не подходит для этого продукта'],
            }
        )
        self.assertEqual(ProductComplaint.objects.all().count(), 0)

    def test_empty_product(self):
        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'product': '',
                'portion': self.bread_portion_100gr.id,
                'text': 'Это жалоба'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'product': ['Это поле не может быть пустым.']})

        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'portion': self.bread_portion_100gr.id,
                'text': 'Это жалоба'
            }
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'product': ['Обязательное поле.']})

        self.assertEqual(ProductComplaint.objects.all().count(), 0)

    def test_empty_portion(self):
        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'product': self.bread_product.slug,
                'portion': '',
                'text': 'Это жалоба'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'portion': ['Это поле не может быть пустым.']})

        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'product': self.bread_product.slug,
                'text': 'Это жалоба'
            }
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'portion': ['Обязательное поле.']})

        self.assertEqual(ProductComplaint.objects.all().count(), 0)

    def test_empty_text(self):
        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'product': self.bread_product.slug,
                'portion': self.bread_portion_100gr.id,
                'text': ''
            }
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'text': ['Это поле не может быть пустым.']})

        response = self.client.post(
            '/api/products/create-complaint/',
            {
                'product': self.bread_product.slug,
                'portion': self.bread_portion_100gr.id,
            }
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'text': ['Обязательное поле.']})

        self.assertEqual(ProductComplaint.objects.all().count(), 0)

    def test_empty_body(self):
        response = self.client.post('/api/products/create-complaint/', {})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {
                'product': ['Обязательное поле.'],
                'portion': ['Обязательное поле.'],
                'text': ['Обязательное поле.']
            }
        )

        self.assertEqual(ProductComplaint.objects.all().count(), 0)
