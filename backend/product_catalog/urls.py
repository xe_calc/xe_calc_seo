from django.urls import path, include

from product_catalog.views import (
    IndexPage,
    ProductPage,
    BrandPage,
    SearchProduct,
    ProductComplaintCreate,
    FeedBackView,
)

app_name = 'catalog'
urlpatterns = [
    path('', IndexPage.as_view(), name='index'),
    path('search/', SearchProduct.as_view(), name='search'),
    path('feedback/', FeedBackView.as_view(), name='feedback'),
    path('complaint/<slug:product>/', ProductComplaintCreate.as_view(), name='complaint_create'),
    path('brand/<slug:brand>/', BrandPage.as_view(), name='brand_detail'),
    path('api/', include('api.urls')),
    path('<slug:product>/', ProductPage.as_view(), name='product_detail'),

]
