$(document).ready(function(){
    function set_products(products){
        $('#product_res').empty();
        if (products.length > 0) {
            $(products).each(function (index, item){
                console.log(item);
                $('#product_res').append('<a href="'+ item["url"] +'" class="label label-info">'+ item["name"] +'</a> ');
            })

        }
    }
    $('[name="search"]').on('keyup', function(){
       let q = $(this).val();
       if(q.length >= 3) {
           $.get('/search/', {q:q} )
               .done(function(data) {
                   set_products(data['result']);
               })
               .error(function (data){
                   console.log(data);
               });
       }
       else {
           set_products([])
       }

    });

});