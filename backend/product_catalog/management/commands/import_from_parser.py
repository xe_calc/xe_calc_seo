import json
from decimal import Decimal, InvalidOperation

from django.core.management.base import BaseCommand
from pytils.translit import slugify

from product_catalog.models import Product, ProductBrand, ProductPortion


class Command(BaseCommand):

    def create_product(self, product):
        name = product['name'].strip()
        brand = ''
        if product['manufacturer']:
            brand = product['manufacturer'].strip()
            name_slug = slugify(f'{name} ({brand})')
        else:
            name_slug = slugify(name)

        product_db, create = Product.objects.get_or_create(slug=name_slug, defaults={'name': name})

        if brand and not product_db.brand:
            brand_slug = slugify(brand)
            brand, created = ProductBrand.objects.get_or_create(
                slug=brand_slug,
                defaults={'name': brand}
            )
            product_db.brand = brand
            product_db.save()
        return product_db

    def handle(self, *args, **options):
        imported = 0
        Product.objects.all().delete()
        ProductBrand.objects.all().delete()
        with open('products.json') as f:
            products = json.loads(f.read())
            for product in products:
                product_db = self.create_product(product)

                portion_name = product['portion'].strip()
                if 'унция' not in portion_name:
                    if not ProductPortion.objects.filter(product=product_db, name=portion_name).exists():
                        portion = ProductPortion()
                        portion.product = product_db
                        portion.name = portion_name
                        portion.protein = Decimal(product['protein'])
                        portion.calories = Decimal(product['calories'])
                        portion.fat = Decimal(product['fat'])
                        portion.carbohydrates = Decimal(product['carbohydrates'])

                        portion.xe_count = round(Decimal(product['carbohydrates']) / 12, 2)

                        if portion_name in ['100 г', '100 мл']:
                            portion.for_weighing = True
                        try:
                            portion.save()
                        except InvalidOperation:
                            pass
                        imported += 1
                print(imported)
