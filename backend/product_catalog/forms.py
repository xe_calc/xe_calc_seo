from django import forms

from product_catalog.models import Feedback


class ProductCompliantForm(forms.Form):
    portion = forms.ChoiceField(label='Порция')
    text = forms.CharField(widget=forms.Textarea, label='Описание проблемы', required=True)

    def __init__(self, *args, **kwargs):
        product = kwargs.pop('product')
        super().__init__(*args, **kwargs)

        portions_choices = product.portions.all().values_list('id', 'name')
        self.fields['portion'].choices = [(str(portion[0]), portion[1]) for portion in portions_choices]

    class Meta:
        fields = ['portion', 'text']


class FeedBackForm(forms.ModelForm):

    class Meta:
        model = Feedback
        fields = ['contacts', 'text']
