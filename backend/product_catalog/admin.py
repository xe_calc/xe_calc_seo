from django.contrib import admin

from product_catalog.models import (
    ProductBrand,
    Product,
    ProductPortion,
    TopBrand,
    TopProduct,
    ProductComplaint,
    Feedback
)


class PortionInline(admin.StackedInline):
    model = ProductPortion


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [PortionInline]
    search_fields = ['name']


@admin.register(TopProduct)
class TopProductAdmin(admin.ModelAdmin):
    raw_id_fields = ['product']


@admin.register(ProductComplaint)
class ProductCompliant(admin.ModelAdmin):
    raw_id_fields = ['product', 'portion']


admin.site.register(ProductBrand)
admin.site.register(ProductPortion)
admin.site.register(TopBrand)
admin.site.register(Feedback)
