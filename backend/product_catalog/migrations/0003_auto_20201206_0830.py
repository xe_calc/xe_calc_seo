# Generated by Django 3.1.3 on 2020-12-06 05:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product_catalog', '0002_auto_20201202_0932'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contacts', models.CharField(max_length=300, verbose_name='Контакты')),
                ('text', models.TextField(max_length=300, verbose_name='Текст')),
            ],
            options={
                'verbose_name': 'Жалоба на продукт',
                'verbose_name_plural': 'Жалобы на продукты',
            },
        ),
        migrations.AlterModelOptions(
            name='topmanufacturer',
            options={'verbose_name': 'Бренд для главной', 'verbose_name_plural': 'Бренды для главной'},
        ),
        migrations.CreateModel(
            name='ProductComplaint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Текст жалобы')),
                ('solved', models.BooleanField(default=False)),
                ('portion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='product_catalog.productportion', verbose_name='Порция')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='complaints', to='product_catalog.product', verbose_name='Продукт')),
            ],
            options={
                'verbose_name': 'Жалоба на продукт',
                'verbose_name_plural': 'Жалобы на продукты',
            },
        ),
    ]
