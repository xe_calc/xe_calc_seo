import uuid as uuid

from django.db import models
from django.urls import reverse


class ProductBrand(models.Model):
    name = models.CharField('Название', max_length=250)
    slug = models.CharField('Машинное название', max_length=200, unique=True)

    def get_url(self):
        return reverse('catalog:brand_detail', kwargs={'brand': self.slug})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Производитель'
        verbose_name_plural = 'Производитель'


class Product(models.Model):
    uuid = models.UUIDField('uid', unique=True, default=uuid.uuid4)
    name = models.CharField('Название', max_length=300, db_index=True)
    slug = models.CharField('Машинное название', max_length=200, unique=True)
    brand = models.ForeignKey(ProductBrand, on_delete=models.CASCADE, null=True, blank=True,
                              related_name='products')

    def get_url(self):
        return reverse('catalog:product_detail', kwargs={'product': self.slug})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'


class ProductPortion(models.Model):
    name = models.CharField('Название', max_length=250)
    for_weighing = models.BooleanField('Продукт доступен для взвешивания', default=False)
    calories = models.DecimalField('Калории', decimal_places=2, max_digits=5)
    fat = models.DecimalField('Жиры', decimal_places=2, max_digits=5)
    protein = models.DecimalField('Белки', decimal_places=2, max_digits=5)
    carbohydrates = models.DecimalField('Углеводы', decimal_places=2, max_digits=5)
    xe_count = models.DecimalField('Количество хлебных единиц', decimal_places=2, max_digits=5)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='portions')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Порция продукта'
        verbose_name_plural = 'Порции продуктов'


class TopProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='top_products')
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.product.name

    class Meta:
        verbose_name = 'Продукт для главной'
        verbose_name_plural = 'Топ продуктов для главной'


class TopBrand(models.Model):
    brand = models.ForeignKey(ProductBrand, on_delete=models.CASCADE, related_name='top_brands')
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.brand.name

    class Meta:
        verbose_name = 'Бренд для главной'
        verbose_name_plural = 'Бренды для главной'


class ProductComplaint(models.Model):
    product = models.ForeignKey(Product, verbose_name='Продукт', on_delete=models.CASCADE,
                                related_name='complaints')
    portion = models.ForeignKey(ProductPortion, verbose_name='Порция', on_delete=models.CASCADE)
    text = models.TextField('Текст жалобы')
    solved = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Жалоба на продукт'
        verbose_name_plural = 'Жалобы на продукты'


class Feedback(models.Model):
    contacts = models.CharField('Контакты', max_length=300)
    text = models.TextField('Текст', max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.text)

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
