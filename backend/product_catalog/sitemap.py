from django.conf import settings
from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from product_catalog.models import Product, ProductBrand


class ProductSitemap(Sitemap):
    limit = 10000
    changefreq = 'monthly'
    priority = 1.0
    lastmod = settings.LAST_UPDATE_CATALOG_DT

    def items(self):
        return Product.objects.all().order_by('id')

    def location(self, obj):
        return obj.get_url()


class BrandSitemap(Sitemap):
    limit = 10000
    changefreq = 'monthly'
    priority = 0.5
    lastmod = settings.LAST_UPDATE_CATALOG_DT

    def items(self):
        return ProductBrand.objects.all().order_by('id')

    def location(self, obj):
        return obj.get_url()


class StaticViewSitemap(Sitemap):
    priority = 0.2
    changefreq = 'monthly'

    def items(self):
        return ['catalog:index', 'catalog:feedback']

    def location(self, item):
        return reverse(item)
