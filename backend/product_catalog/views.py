from django.conf import settings
from django.contrib import messages
from django.contrib.postgres.search import SearchVector
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, DetailView, View, FormView, CreateView, ListView

from product_catalog.forms import ProductCompliantForm, FeedBackForm
from product_catalog.models import TopProduct, TopBrand, Product, ProductBrand, ProductPortion, \
    ProductComplaint


class IndexPage(TemplateView):
    template_name = 'index.html'

    def _get_top_products(self):
        result = []
        product_tops = TopProduct.objects.filter(
            active=True
        ).select_related(
            'product'
        ).only(
            'product__name',
            'product__slug'
        )
        for product_top in product_tops:
            result.append({
                'name': product_top.product.name,
                'url': product_top.product.get_url()
            })
        return result

    def _get_top_brands(self):
        result = []
        top_brands = TopBrand.objects.filter(
            active=True
        ).select_related(
            'brand'
        ).only(
            'brand__name',
            'brand__slug'
        )
        for top_brand in top_brands:
            result.append({
                'name': top_brand.brand.name,
                'url': top_brand.brand.get_url()
            })
        return result

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['top_products'] = self._get_top_products()
        ctx['top_brands'] = self._get_top_brands()
        return ctx


class ProductPage(DetailView):
    template_name = 'product_detail.html'
    queryset = Product.objects.all()
    slug_url_kwarg = 'product'
    context_object_name = 'product'


class BrandPage(ListView):
    template_name = 'brand_detail.html'
    queryset = Product.objects.all()
    context_object_name = 'products'
    paginate_by = 50

    def get_queryset(self):
        self.brand = get_object_or_404(ProductBrand, slug=self.kwargs['brand'])
        return self.queryset.filter(brand=self.brand).order_by('-name')

    def get_context_data(self, *, object_list=None, **kwargs):
        ctx = super(BrandPage, self).get_context_data(object_list=object_list, **kwargs)
        ctx['brand'] = self.brand
        return ctx


class SearchProduct(View):

    def get(self, request, *args, **kwargs):
        name = request.GET.get('q')
        products = []
        if name and len(name) >= 3:

            qs = (
                Product.objects
                .annotate(search=SearchVector('name', 'brand__name'))
                .filter(search=name)
                .select_related('brand')
                .only('name', 'slug', 'brand__name')
                .order_by('-brand')
                [:settings.PRODUCT_MAX_COUNT_FOR_SEARCH]
            )

            for product in qs:
                if product.brand:
                    name = f'{product.name} ({product.brand.name})'
                else:
                    name = product.name
                products.append({'name': name, 'url': product.get_url()})
        return JsonResponse({'result': products})


class ProductComplaintCreate(FormView):
    template_name = 'product_complaint.html'
    form_class = ProductCompliantForm
    product = None

    def get_product(self):
        return get_object_or_404(Product, slug=self.kwargs.get('product'))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.product = self.get_product()
        kwargs['product'] = self.product
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['product'] = self.product
        return ctx

    def form_valid(self, form):
        portion = get_object_or_404(ProductPortion, id=form.cleaned_data['portion'])
        ProductComplaint.objects.create(product=self.product, portion=portion, text=form.cleaned_data['text'])
        messages.add_message(self.request, messages.INFO, 'Ваше обращение отправлено.')
        return HttpResponseRedirect(self.product.get_url())


class FeedBackView(CreateView):
    template_name = 'feedback.html'
    form_class = FeedBackForm

    def form_valid(self, form):
        form.save()
        messages.add_message(self.request, messages.INFO, 'Ваше обращение отправлено.')
        return HttpResponseRedirect('/')
